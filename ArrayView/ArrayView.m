//
//  ArrayView.m
//
//  Created by EvgenyMikhaylov on 9/1/15.
//
//

#import "ArrayView.h"
#import "UIView+LayoutMargins.h"

@interface ArrayView ()

@property (nonatomic) NSArray *views;
@property (nonatomic) CGFloat spacing;
@property (nonatomic) UILayoutConstraintAxis axis;
@property (nonatomic) ArrayViewAlignment alignment;
@property (nonatomic) UIView *contentView;

@end

@implementation ArrayView

+ (instancetype)arrayViewWithViews:(NSArray *)views
                           spacing:(CGFloat)spacing
                              axis:(UILayoutConstraintAxis)axis
                         alignment:(ArrayViewAlignment)alignment{
    return [[ArrayView alloc] initWithViews:views spacing:spacing axis:axis alignment:alignment];
}

- (instancetype)initWithViews:(NSArray *)views
                      spacing:(CGFloat)spacing
                         axis:(UILayoutConstraintAxis)axis
                    alignment:(ArrayViewAlignment)alignment
{
    self = [super init];
    if (self) {
        
        self.layoutMargins = UIEdgeInsetsZero;
        self.flexibleLayoutMargins = YES;
        self.spacing = spacing;
        self.axis = axis;
        self.alignment = alignment;
        
        self.contentView = [[UIView alloc] init];
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.contentView];
        NSDictionary *viewNames = NSDictionaryOfVariableBindings(_contentView);
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_contentView]-|" options:0 metrics:nil views:viewNames]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_contentView]-|" options:0 metrics:nil views:viewNames]];
        
        [self setViews:views.copy];
    }
    return self;
}

- (void)setViews:(NSArray *)views{
    
    for (UIView *view in _views) {
        [view removeFromSuperview];
    }
    
    _views = views;
    
    if (_views.count == 0){
        return;
    }
    
    NSMutableDictionary *viewsDictionary = [[NSMutableDictionary alloc] init];
    NSMutableString *axisString = [[NSMutableString alloc] init];
    [axisString appendString:[self visualFormatPrefixForAxis:self.axis]];
    [axisString appendString:@"|"];
    [_views enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        CGFloat spacing = (obj == _views.firstObject) ? 0.0f : _spacing;
        UIView *containerView = [self containerViewWithView:obj spacing:spacing];
        NSString *viewKey = [NSString stringWithFormat:@"view%@", @(idx)];
        [viewsDictionary setObject:containerView forKey:viewKey];
        [axisString appendString:[NSString stringWithFormat:@"[%@]",viewKey]];
        NSMutableString *firstNonAxisString = [[NSMutableString alloc] init];
        [firstNonAxisString appendString:[self visualFormatPrefixForAxis:self.nonActiveAxis]];
        [firstNonAxisString appendString:[NSString stringWithFormat:@"|-(>=0)-[%@]-(>=0)-|",viewKey]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:firstNonAxisString
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:viewsDictionary]];
        NSMutableString *secondNonAxisString = [[NSMutableString alloc] init];
        [secondNonAxisString appendString:[self visualFormatPrefixForAxis:self.nonActiveAxis]];
        [secondNonAxisString appendString:[NSString stringWithFormat:@"|-(==0@1)-[%@]-(==0@1)-|",viewKey]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:secondNonAxisString
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:viewsDictionary]];
    }];
    [axisString appendString:@"|"];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:axisString
                                                                             options:[self layoutFormatOptionsForAxis:self.axis]
                                                                             metrics:nil
                                                                               views:viewsDictionary]];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self updateFirstViewMargins];
}

- (void)updateFirstViewMargins{
    UIView *firstView = nil;
    for (UIView *view in self.views) {
        [view layoutIfNeeded];
        CGFloat dimension = [self sizeDimension:view.frame.size forAxis:self.axis];
        if (dimension > 0){
            if (!firstView){
                firstView = view;
                [self setSpacing:0.0f forView:firstView.superview];
            }
            else{
                [self setSpacing:self.spacing forView:view.superview];
            }
        }
    }
}

#pragma mark - Helpers

- (UIView*)containerViewWithView:(UIView*)view spacing:(CGFloat)spacing{
    
    UIView *containerView = [[UIView alloc] init];
    containerView.flexibleLayoutMargins = YES;
    [self setSpacing:spacing forView:containerView];
    containerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:containerView];
    
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:view];
    
    NSDictionary *viewNames = NSDictionaryOfVariableBindings(view);
    [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[view]-|" options:0 metrics:nil views:viewNames]];
    [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[view]-|" options:0 metrics:nil views:viewNames]];
    
    return containerView;
}

- (NSString*)visualFormatPrefixForAxis:(UILayoutConstraintAxis)axis{
    switch (axis) {
        case UILayoutConstraintAxisHorizontal:      return @"H:";        break;
        case UILayoutConstraintAxisVertical:        return @"V:";        break;
    }
}

- (UILayoutConstraintAxis)nonActiveAxis{
    switch (self.axis) {
        case UILayoutConstraintAxisHorizontal:      return UILayoutConstraintAxisVertical;          break;
        case UILayoutConstraintAxisVertical:        return UILayoutConstraintAxisHorizontal;        break;
    }
}

- (CGFloat)sizeDimension:(CGSize)size forAxis:(UILayoutConstraintAxis)axis{
    switch (axis) {
        case UILayoutConstraintAxisHorizontal:      return size.width;          break;
        case UILayoutConstraintAxisVertical:        return size.height;        break;
    }
}

- (NSLayoutFormatOptions)layoutFormatOptionsForAxis:(UILayoutConstraintAxis)axis{
    switch (self.alignment) {
        case ArrayViewAlignmentLeft:
            return (axis == UILayoutConstraintAxisVertical) ? NSLayoutFormatAlignAllLeft : NSLayoutFormatAlignAllTop;
            break;
        case ArrayViewAlignmentRight:
            return (axis == UILayoutConstraintAxisVertical) ? NSLayoutFormatAlignAllRight : NSLayoutFormatAlignAllBottom;
            break;
        case ArrayViewAlignmentCenter:
            return (axis == UILayoutConstraintAxisVertical) ? NSLayoutFormatAlignAllCenterX : NSLayoutFormatAlignAllCenterY;
            break;
    }
}

- (void)setSpacing:(CGFloat)spacing forView:(UIView*)view{
    switch (self.axis) {
        case UILayoutConstraintAxisHorizontal:
            view.layoutMargins = UIEdgeInsetsMake(0.0f, spacing, 0.0f, 0.0f);
            break;
        case UILayoutConstraintAxisVertical:
            view.layoutMargins = UIEdgeInsetsMake(spacing, 0.0f, 0.0f, 0.0f);
            break;
        default:
            break;
    }
}

@end