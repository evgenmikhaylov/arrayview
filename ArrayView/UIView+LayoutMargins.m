//
//  UIView+LayoutMargins.m
//
//  Created by EvgenyMikhaylov on 9/2/15.
//
//

#import "UIView+LayoutMargins.h"
#import <objc/runtime.h>

@interface UIView ()

@property (nonatomic) UIEdgeInsets layoutMarginsCache;

@end

@implementation UIView (LayoutMargins)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        SEL originalSelector = @selector(layoutSubviews);
        SEL swizzledSelector = @selector(lm_layoutSubviews);
        Method originalMethod = class_getInstanceMethod(class, originalSelector);
        Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
        BOOL didAddMethod = class_addMethod(class,
                                            originalSelector,
                                            method_getImplementation(swizzledMethod),
                                            method_getTypeEncoding(swizzledMethod));
        if (didAddMethod) {
            class_replaceMethod(class, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
        }
        else {
            method_exchangeImplementations(originalMethod, swizzledMethod);
        }
    });
}

- (void)lm_layoutSubviews{
    [self lm_layoutSubviews];
    
    if (!self.flexibleLayoutMargins)
        return;
    
    BOOL needsClearLayoutMargins = NO;
    UIEdgeInsets margins = self.layoutMargins;
    if (CGRectGetHeight(self.bounds) <= (self.layoutMargins.top + self.layoutMargins.bottom)){
        margins.top = 0.0f;
        margins.bottom = 0.0f;
        needsClearLayoutMargins = YES;
    }
    if (CGRectGetWidth(self.bounds) <= (self.layoutMargins.left + self.layoutMargins.right)){
        margins.left = 0.0f;
        margins.right = 0.0f;
        needsClearLayoutMargins = YES;
    }
    if (needsClearLayoutMargins){
        self.layoutMarginsCache = self.layoutMargins;
        self.layoutMargins = margins;
        [self setNeedsLayout];
    }
    if (!needsClearLayoutMargins && !UIEdgeInsetsEqualToEdgeInsets(self.layoutMarginsCache, UIEdgeInsetsZero)){
        self.layoutMargins = self.layoutMarginsCache;
        self.layoutMarginsCache = UIEdgeInsetsZero;
        [self setNeedsLayout];
    }
}

- (void)setLayoutMarginsCache:(UIEdgeInsets)layoutMarginsCache{
    objc_setAssociatedObject(self, @selector(layoutMarginsCache), [NSValue valueWithUIEdgeInsets:layoutMarginsCache], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIEdgeInsets)layoutMarginsCache{
    return [objc_getAssociatedObject(self, @selector(layoutMarginsCache)) UIEdgeInsetsValue];
}

- (void)setFlexibleLayoutMargins:(BOOL)flexibleLayoutMargins{
    objc_setAssociatedObject(self, @selector(flexibleLayoutMargins), @(flexibleLayoutMargins), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)flexibleLayoutMargins{
    return [objc_getAssociatedObject(self, @selector(flexibleLayoutMargins)) boolValue];
}

@end
